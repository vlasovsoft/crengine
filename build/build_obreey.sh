#!/bin/sh
export TOOL=/home/sergv/toolchain/obreey
export PATH=$TOOL/bin:$TOOL/qt4/bin:$PATH
rm -rf qtbuild_obreey
mkdir qtbuild_obreey
cd qtbuild_obreey
cmake -D CMAKE_TOOLCHAIN_FILE=../../tools/toolchain-arm-obreey.cmake -D GUI=QT -D CMAKE_BUILD_TYPE=Release -D MAX_IMAGE_SCALE_MUL=2 -D DOC_DATA_COMPRESSION_LEVEL=3 -D DOC_BUFFER_SIZE=0x1400000 -D CMAKE_INSTALL_PREFIX=/usr ../..
make
