#!/bin/sh
set -e
export LC_ALL=C
for arch in kobo kindle obreey qvfb; do
    ./build_$arch.sh
done
xclock

